<?php

class FlickrApi {
    
    private $url = 'https://api.flickr.com/services/feeds/photos_public.gne';
    
    public function findPicByTag($tag) {
        $requesUrl = $this->url . '?tags=' . $tag;
        
        return self::httpRequest($requesUrl);
    }
    
    /**
     * call request over curl
     * 
     * @param type $url
     * @return type
     */
    private function httpRequest($url) {
        $res = null;
        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch);
        curl_close($ch);
        
        try {
            $res = self::parseXmlResponse($output);
        } catch (Exception $e) {
            $res = ['error' => $e->getMessage()];
        }

        return json_encode($res);
    }
    
    /**
     * Parse request
     * 
     * @param type $req
     * @return type
     * @throws Exception
     */
    private function parseXmlResponse($req) {
        $xml = simplexml_load_string($req);
        $picures = [];
        
        if ($xml === false) {
            throw new Exception('Api response is empty');
        } else {
            foreach($xml->entry as $entrys) {
                foreach ($entrys->link as $link) {
                    if ($link['type'] == 'image/jpeg') { 
                        $picures[] = (string)$link['href'];
                    }
                }
            }
        }
            
        return $picures;
    }
}

$flickr = new FlickrApi();

if (isset($_POST['phase']) && !empty($_POST['phase'])) {
    echo $flickr->findPicByTag($_POST['phase']);    
}
