const gulp = require('gulp'),
        minify = require('gulp-minify-css'),
        rename = require('gulp-rename'),
        using = require('gulp-using'),
        uglify = require('gulp-uglify'),
        babel = require('gulp-babel'), // ES6 -> es2015
        concat = require('gulp-concat'),
        sass = require('gulp-sass');

// Compile sass into CSS
gulp.task('bootstrap-sass', function() {
    return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/custom.scss'])
            .pipe(sass())
            .pipe(minify({keepBreaks: false}))
            .pipe(using())
            .pipe(rename({
                suffix: '.min'
            }))

            .pipe(gulp.dest('media/css/'));
});

// Move the javascript files into our /src/js folder
gulp.task('bootstrap-js', function() {

    return gulp.src(
            ['node_modules/bootstrap/dist/js/bootstrap.min.js',
                'node_modules/jquery/dist/jquery.min.js',
                'node_modules/tether/dist/js/tether.min.js'
            ]
            ).pipe(gulp.dest('media/js/'));
});

gulp.task('js-custom', function() {

    return gulp.src(['src/js/*.js'])
            .pipe(babel({presets: ['es2015']})) // translate es6->escam2015
            .pipe(uglify())
            .on('error', function (err) {
                console.log(err.toString())
            })
            .pipe(using())
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(concat('custom.min.js'))
            .pipe(gulp.dest('media/js/'));
});

// Static Server + watching scss/html files
gulp.task('serve', ['bootstrap-sass'], function() {
    gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'], ['bootstrap-sass']);
    gulp.watch(['src/js/*.js'], ['js-custom']);
});

gulp.task('default', ['bootstrap-js', 'js-custom', 'serve']);
