<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Search in Flickr</title>
    </head>
    <body>
        <div class="container">
            <div id="alert" class="alert alert-danger alert-dismissible fade show my-2" role="alert">
                There is problem with call to API refresh site and try again.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="jumbotron mt-3 text-center">
                <h1 class="">Search in Flickr</h1>
                <p class="lead">Find pictures on flickr.com by a tag</p>
                <form>
                    <input id="textToFind" class="form-control form-control-lg" type="text" placeholder="Put a phrase to find it">
                </form>
            </div>
        </div>

        <div id="containerForPhoto" class="container-fluid photo-search-container">
            <div class="row my-2 align-middle">
                <div id="pic0" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic1" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic2" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic3" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic4" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic5" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic6" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic7" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic8" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic9" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic10" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic11" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic12" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic13" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic14" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic15" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic16" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic17" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic18" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic19" class="col-md-3 col-sm-4 col-6"></div>
                <div id="pic20" class="col-md-3 col-sm-4 col-6"></div>
            </div>
        </div>

        <link href="/media/css/custom.min.css" rel="stylesheet" type="text/css"/>
        <script src="/media/js/jquery.min.js"></script>
        <script src="/media/js/bootstrap.min.js"></script>
        <script src="media/js/custom.min.js"></script>
    </body>
</html>
