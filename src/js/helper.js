$(document).ready(function() {
    'use strict';
    
    $('#textToFind').on('keyup', function(e) {
        e.preventDefault();
        
        let phase = $(this).val();
        
        ///prevent to send ajax with empty string
        if (!phase.length) {
            for (let i=0; i<20 ;i++) {    
                $('#pic' + i).empty()
            }
            
            return;
        }
                
        $.ajax({
            type: 'POST',
            url: 'flickrApi.php',
            data: {
                phase: phase,
            },
            
            success: (ret) => {
                let jsonData = JSON.parse(ret);
                
                if (jsonData.error) {
                    $('#alert').css('display', 'block')
                    
                    return;
                } 

                $.each(jsonData, (i, val) => {
                    $('#pic' + i).empty().html('<a target=\"_blank\" href=\"' + val + '\"><img class=\"img-thumbnail mx-auto d-block my-2\" src="' + val + '"/></a>');
                });
            },
            
            error: (jqXHR, errorText, errorThrown) => {
                $('#alert').css('display', 'block')
            }
        });
    })
});
